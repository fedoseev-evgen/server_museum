var nodemailer = require('nodemailer'); 
const log = require('./logs')(module);
module.exports = function (service, from, passsword, subject, text, to,calback) {
    var transporter = nodemailer.createTransport({
        service: service,
        auth: {
            user: from,
            pass: passsword
        }
    });
    var mailOptions = {
        from: from,
        to: to,
        subject: subject,
        text: text
    }
    transporter.sendMail(mailOptions, function(err, info){
        if (err) {
            log.error(err);
            calback(err);
        } else {
            calback('Сообщение успешно отправлено: ' + info.response);
        }
    });
}