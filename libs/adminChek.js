const jwt = require('jsonwebtoken');
const config = require('./config');
const log = require('./logs')(module);
const admin = require('./../scheme/admin');
module.exports = function verify(req, res, next){
    try{
        console.log(req.cookies);
        jwt.verify(req.cookies.token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                admin.findOne({token:decoded.token},(err,result) => {
                    if(result!== null && !err)
                    next();
                    else{
                        res.send('Вы не авторизированны');
                    }
                });
            } else  if (err) {
                res.send('Вы не авторизированны');
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.send('Вы не авторизированны');
        } else {
            log.error(err)
            res.send(err.message);
        }
    }
}
