const fs = require('fs');

module.exports = function fileUnlink(file, callback){

    fs.exists(file, function(exist){
        if (exist){
            fs.unlink(file, function(err){
                if (err){
                    callback('Could not remove file');
                } else {
                    callback(null, 'Success');
                }
            })
        } else {
            callback(null, 'Success');
        }
    });
};