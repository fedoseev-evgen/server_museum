var config = {};
const ExtractJwt = require("passport-jwt").ExtractJwt;
config.adress= "vz270076.eurodir.ru";
config.port = 80;
config.name = "API";

config.jwt= {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('bearer'),

    secretOrKey: '7DDJSu8UAvwfzRwjNgB5K4q4',

    expiresIn: 60 * 30 * 1000,

    expiresInRefreshToken: 1000 * 60*60*24*90,
},


config.mongoose = {
    url: "mongodb://127.0.0.1/server",
};


module.exports=config;