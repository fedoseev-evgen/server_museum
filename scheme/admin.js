const mongoose = require('mongoose');
const crypto = require('crypto');
const adminSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        required:true,
        type:String,
    },
    hashPassword:{
        type:String,
        required:true
    },
    salt:{
        type:String,
        required:true
    },
    token:{
        type:String,
        default:null,
    }
},{
    collection: "admin",
    versionKey: false
});

adminSchema.virtual('password')
    .set(function (password) {
        this.salt = Math.random() + 'asdasdasd';
        this.hashPassword = this.encryptPassword(password);
    });


adminSchema.methods = {
    encryptPassword: function (password) {
        return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
    },
    checkPassword: function (password) {
        return this.encryptPassword(password) === this.hashPassword;
    }
};    

adminSchema.statics = {
    checkName:async function (name) {
        const _name = await this.findOne({name: name}).exec();
        if (_name === null){
            return true;
        } else {
            return false;
        }
    }
}
adminSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('admin', adminSchema);
