const Institution = require('../../scheme/institution');
const saveFiles = require('../../libs/saveFiles');

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        Institution.findById(req.body._id).exec((err, result) => {
            var editRes = {};
            for (var key in req.body) {
                if (key !== "_id")
                    editRes[key] = req.body[key]
            }
            result.set(editRes);
            result.save((err, inst) => {
                if (err) {
                    res.send(err)
                }
                else {
                    if (req.files[0] !== undefined) {
                        saveFiles('/', req.files[0], inst._id + '', (e, f) => {
                            if (e) {
                                res.send("Учреждение успешно добавленно, но с фото произошла ошибка!")
                            } else {
                                result.set({mainImg: f});
                                result.save((ee) => {
                                    if (ee) res.send("Учреждение успешно добавленно, но с фото произошла ошибка!")
                                    else res.send("Учреждение успешно добавленно!");
                                })
                            }
                        });
                    }
                    else res.send("Учреждение успешно добавленно!");
                }
            });
        });
    }
    catch (err) {
        log.error(err);
        res.send(err);
    }
};
