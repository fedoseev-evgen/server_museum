const Admin = require('../../../scheme/admin');
const log = require('../../../libs/logs')(module);
module.exports=async function (req, res){
    if(await Admin.checkName(req.body.name)){
        if(req.body.name!==undefined&&req.body.name.length>4&&req.body.name.length<=25){
            if(req.body.password!==undefined&&req.body.password.length>4&&req.body.password.length<=25){
                var newAdmin = new Admin({
                    name: req.body.name,
                    password: req.body.password,
                });
                newAdmin.save(function(err , admin) {
                    if(err){
                        log.error(err);
                        res.send(err);
                    }else{
                        res.send(admin);
                    }
                }); 
            }else res.send("Заполните поле пароль от 5 до 25 символов");
        }else res.send("Заполните поле имя от 5 до 25 символов");
    }else res.send("Данное имя уже занято");
}
        