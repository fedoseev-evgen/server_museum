const veteran = require('../../scheme/veteran');
const log = require('../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        console.log(req.body);
        veteran.findById(req.body._id).exec((err, result) => {
            let mass = result.media;
            for (let i = 0; i < mass.video.length; i++) {
                if (mass.video[i].id.toString() === req.body.id.toString()) {
                    mass.video.splice(i, 1);
                    break;
                }
            }
            result.set({media: mass});
            result.save((err, newInst) => {
                if (err) {
                    log.error(err);
                    res.send("Произошла ошибка!");
                } else {
                    res.send("Удаление прошло успешно!");
                }
            })
        })
    }
    catch (e) {
        log.error(e);
        res.send("Ошибка!");
    }
};