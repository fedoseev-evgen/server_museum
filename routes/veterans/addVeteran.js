const Veteran = require('../../scheme/veteran');
const log = require('../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        var veteran = new Veteran({
            name: req.body.name,
        });
        veteran.save((err, inst) => {
            if (err) {
                res.send("Ветеран не добавлен")
            }
            else {
                res.send("Ветеран успешно добавлен!");
            }
        });
    }
    catch {
        log.error(err);
        res.send("Ветеран не добавлен");
    }
};
