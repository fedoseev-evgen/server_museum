const veteran = require('./../../scheme/veteran');
const log = require('./../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        console.log(req.body);
        veteran.findById(req.body._id).exec((err, result) => {
            if (result === null || result === undefined) {
                res.send("Ветеран не найден!");
            } else {
                result.set({status:true});
                result.save((err) => {
                    if (err) {
                        log.error(err);
                        res.send("Произошла ошибка!");
                    }
                    else {
                        res.send("Ветерана теперь видно!");
                    }
                })
            }

        })
    } catch (e) {
        log.error(e);
        res.send("Произошла ошибка");
    }
};