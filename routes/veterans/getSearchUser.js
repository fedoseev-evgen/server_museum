const veteran = require('./../../scheme/veteran');
const log = require('./../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        veteran.find({
            status:true,
            name: {$regex: req.body.name, $options: 'i'}
        }).exec((err, result) => {
            if (err) {
                log.error(err);
                res.send([])
            } else if (result === null) {
                res.send([])
            } else {
                res.send(result)
            }
        });
    }
    catch (e) {
        log.error(e);
        res.send([]);
    }
};