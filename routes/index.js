const adminChek = require('./../libs/adminChek');

module.exports = async function(app){  
    app.get('/sort/getAll',require('./sort/getAll'));
    app.post('/sort/getWithPar',require('./sort/getWithPar'));
    app.post('/sort/getById',require('./sort/getById'));


    app.post('/status/true/byId',adminChek,require('./Institution/true'));
    app.post('/status/false/byId',adminChek,require('./Institution/false'));


    app.post('/singUpAdmin', require('./admin/singUpAdmin'));
    app.post('/singInAdmin', require('./admin/singInAdmin'));
    app.post('/admin/getAll',adminChek, require('./admin/sort/getAll'));
    app.post('/admin/getById',adminChek, require('./admin/sort/getById'));
    app.post('/admin/getWithPar',adminChek, require('./admin/sort/getWithPar'));




    app.get('/api',require('./api'));
    app.get('/api2',require('./api/api2'));

    // app.post('/mail', require('./mail'));
    app.post('/addInstitution',adminChek, require('./Institution/addInstitution'));
    app.post('/addText',adminChek,require('./Institution/textForInst'));
    app.post('/addSlider',adminChek,require('./Slider/addTitle'));
    app.post('/moreSlider',adminChek,require('./Slider/addSlide'));
    app.post('/editSlide',adminChek,require('./Slider/editSlide'));
    app.post('/delSlide',adminChek,require('./Slider/delSlide'));
    app.post('/editInstitution',adminChek,require('./Institution/editInst'));


    app.post('/addVeteran',adminChek,require('./veterans/addVeteran'));
    app.post('/addPhoto',adminChek,require('./veterans/addPhoto'));
    app.post('/veteran/edit',adminChek,require('./veterans/editVeteran'));
    app.post('/addAddressVideo',adminChek,require('./veterans/addAddressVideo'));
    app.post('/veteran/setTrue',adminChek,require('./veterans/setTrue'));
    app.post('/veteran/setFalse',adminChek,require('./veterans/setFalse'));
    app.post('/veteran/delAddressVideo',adminChek,require('./veterans/delAddressVideo'));
    app.post('/veteran/delPhoto',adminChek,require('./veterans/delPhoto'));
    app.post('/veteran/getAllAdmin',adminChek,require('./veterans/getAllAdmin'));
    app.post('/veteran/getSearchUser',require('./veterans/getSearchUser'));
    app.post('/veteran/getSearchAdmin',adminChek,require('./veterans/getSearchAdmin'));
    app.post('/veteran/getAllUser',require('./veterans/getAllUser'));
    app.post('/veteran/getVetByIdAdmin',adminChek,require('./veterans/getVetByIdAdmin'));
    app.post('/veteran/getVetByIdUser',require('./veterans/getVetByIdUser'));
    app.get('/check',adminChek,(req,res) => {
        res.send('Вы авторизированны! \n OK');});
    app.post('/overlay',require('./Institution/overlay'))
};