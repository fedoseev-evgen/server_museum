const institution = require('../../scheme/institution');
const log = require('../../libs/logs')(module);
const saveFiles = require('../../libs/saveFiles');

module.exports = function (req, res) {
    try {
        institution.findById(req.body._id).exec((err, result) => {
            let mass = result['slider_' + req.body.num].slides;
            console.log(req.body);
            let id = 0;
            for (let index in mass) {
                if (mass[index].id >= id) {
                    id = mass[index].id;
                }
            }
            id++;
            let slide = {
                Name: req.body.Name,
                type1: req.body.type1,
                hoverTxt: req.body.hoverTxt.split('\n'),
                id: id,
                img: '/files/name.jpg',
            };
            mass.push(slide);
            let q = {...result, slides: mass};
            result.set({q});
            result.save((err, newInst) => {
                if (err) {
                    log.error(err);
                    res.send("Произошла ошибка!");
                }
                else {
                    if (req.files[0] !== undefined) {
                        saveFiles('/', req.files[0], newInst._id + "slideid" + id + "slider_" + req.body.num, (e, f) => {
                            if (e) {
                                res.send("Человек успешно добавлен, но с фото произошла ошибка!")
                            } else {
                                let mass_2_0 = newInst["slider_" + req.body.num].slides;
                                mass_2_0[mass_2_0.length - 1].img = f;
                                let box = {...newInst["slider_" + req.body.num], slides: mass_2_0};
                                newInst.set({["slider_" + req.body.num]: box});
                                newInst.save((ee) => {
                                    if (ee) res.send("Человек успешно добавлен, но с фото произошла ошибка!");
                                    else {
                                        res.send("Человек успешно добавлен!");
                                    }
                                })
                            }
                        });
                    }
                    else {
                        res.send("Человек успешно добавлен");
                    }
                }
            })
        })
    }
    catch (e) {
        log.error(e);
        res.send("Ошибка!");
    }
};