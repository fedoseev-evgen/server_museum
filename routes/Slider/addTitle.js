const institution = require('../../scheme/institution');
const log = require('../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        institution.findById(req.body._id).exec((err, result) => {
            if (result === null || result === undefined) {
                res.send("Не найдено заведение!");
            } else {
                let q = result['slider_' + req.body.num];
                q = {...q, title: req.body.title};
                result.set({["slider_" + req.body.num]: q});
                result.save((err) => {
                    if (err) {
                        log.error(err);
                        res.send("Текст слайдера " + req.body.num + " не был обновлен!");
                    }
                    else
                        res.send("Текст слайдера " + req.body.num + " успешно обновлен!");
                })
            }
        })
    }
    catch (e) {
        log.error(e);
        res.send("Ошибка!");
    }
}